#!/usr/bin/env bash

cd src
if [ ! -z $1 ]
then
    if [ $1 == "-?" ] || [ $1 == "-h" ] || [ $1 == "--help" ]
    then
        printf "Usage: start [options]]\n\n"
        printf "Options:\n"
        printf "  -? or -h or --help: Display this information\n"
        printf "  -b or --background: run the server in the background\n"
        printf "\n"
    elif [ $1 == "-b" ] || [ $1 == "--background" ]
    then
        python3 start.py &
    fi
else
    python3 start.py
fi
