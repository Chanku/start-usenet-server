#./Usenet-Server

##Introduction
This is the server respository for ./Usenet! (Pronouced Slash-dot Usenet or as Start-Usenet). Based upon principals of design in both Usenet and on the Website, Sladshdot (/.). This project is designed to bring Slashdot stype moderation and Usenet's distribution and relative persistance. 

##Goals
The goals of this project are simple. Design a way to allow servers to distribute and store files, much like usenet, while allowing for usermoderation like in Slashdot. I also aim to provide security in the form of encrpytion as well. Please note that the current implimentation is likely to change a lot as I work on and fine tune things. 

##Protocals
This project uses the TCP Protocal. Aside from that I'm desining everything myself, although I may swap to NNTP or allow NNTP to be used in the future. Currently I plan on using ports 44400-44405. (This is simply a range, changes are the project won't use more than 44400 and 44401. and the range may change in the future)

##Requirements
This is required for this project:
* Python 3

##Installation
To 'install' this, run: 
'git clone https://github.com/sapein/s-usenet-server

If you are running Linux (or a *nix):
'cd s-usnet-server
'python3 start.py

##FAQ
####1. Who Created this initally
Sapein. He got the idea from DStoo, a contributor to FreeBSD (from the /r/FreeBSD subreddit), along with something similar being mentioned in the /r/kotakuinaction subreddit. 

####2. Why GPL 2.0?
Due to the nature of this project, I want this project to be 100% Free and Open Source Software. Further I would prefer to prevent this becoming Closed Source in the future. However if you need to have it licensed alternatively, I will license it to you under the MIT License if you ask :) . 

NOTE: This number is a bit outdated, I am actually unsure of licensing.

####3. Why not just use USENET/Usenet?
Well a few reasons mainly because I wanted to try and do this. At the very least this will get someone to make a better implimentation. 

####4. Why ./Usenet?
To be honest, it's because I didn't have a better name. ./Usenet is a combination of the abbrivation for Slashdot (/.) and Usenet. To be /.Usenet looked odd and somewhat ugly. So I reversed /. to ./, this is also somewhat funny since ./ is the way you general start up executables in the *nix OS's. However I'm not opposed to changing the name in the future. 

####5. Your code is bad and you should feel bad!
Well that's not a question. However chances are my code IS bad, or at least not as good as it could be, I don't use python all that much and at the time of writing this readme file I'm turning 15 and I'm a going to be a Junior in High School. So my code is probably bad. Feel free to fix it if you want. 
