import socket
import threading
import struct
import sys
import time

# This is probably the ONLY module I can remember and tell what it does.
# It communicates with SNTP servers for the time.

#Function - _get_SNTP_time
#Arguments - None
#Returns - an integer that can be converted into a time string (t), or none
#Description - This is a simple implimentation of the SNTP protocol
def _get_SNTP_time():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    time_since_1970 = 2208988800
    data = ('\x1b' + 47 * '\0').encode('ascii')
    s.sendto(data, ('pool.ntp.org', 123))
    data, address = s.recvfrom(1024)
    if data:
        t = struct.unpack('!12I', data)[10]
        t -= time_since_1970
        return t
    return None

#Function - get_SNTP_time
#Arguments - [OPTIONAL] timeData (Integer/String)
#Returns - a string (time.ctime()) containing the time.
#Description - This is a bit more of a complicated implimentation allowing somebody to supply their own
#                 timeData.
def get_SNTP_time(timeData = ""):
    if timeData == "":
        try:
            t = _get_SNTP_time()
        except socket.gaierror:
            print("Unable to connect to NTP server")
            return 7331
        return time.ctime(t)
    elif timeData != "":
        try:
            t = time.ctime(timeData)
            return t
        except TypeError as e:
            print(e)
            return time.ctime(_get_SNTP_time())

#Function get_raw_SNTP_time
#Arguments - None
#Returns - Integer or None(see _get_SNTP_time)
#Description - A simple wrapper of sorts for _get_SNTP_time.
def get_raw_SNTP_time():
    return _get_SNTP_time()

#Function get_int_time
#Arguments - String Date
#Returns - Integer
#Description - A way to regenerate the Integer data from the post date/time.
def get_int_time(date):
    raise NotImplementedError
