import struct
import socket 

#Seems to be the structs I created. Will probably retool for the server syncronizaiton purpose

#A Temporary Wrapper for _message_sending(), I plan on removing this after I finish up with structs
def message_sending(topic, group, message, isReply, isReplyTo, attachmentP):
    rd = _message_sending(topic, group, message, isReply, isReplyTo, attachmentP)
    return rd 
    
#A temporary wrapper for _message_unpacking. This will be removed along with the other wrappers
def message_unpacking(rd):
    t, g, m, iR, iRT, aP = _message_unpacking(rd)
    return t, g, m, iR, iRT, aP 

def _post_list_sending(groupName):
    
    #This is the code for the Post List sending data structure.
    #The format of the data is designed specifically for ./Usenet
    #The format is:
    #
    #   Length of the Group Name (Unsigned Long), Group Name (string)
    #   Number of Posts (Unsigned Long)
    #
    #Note that this the posts themselves need be be sent seperately.
    #It is in Big Endian Order

    pass

def _post_list_requesting(groupName):
    
    #This is the code for Post List requesting data structure.
    #The format of the data is designed specifically for ./Usenet
    #The format is:
    #
    #   Length of Group Name (Unsigned Long), Group Name (string)
    #
    #It is in Big Endian Order

    pass

def _group_sending(groupName, groupDescription, creationDate = ""):

    #This is the code for the group sending data structure.
    #The format of the data is designed specifically for ./Usenet
    #The format is:
    #
    #   Length of the Group Name (Unsigned Long), Group Name (string)
    #   Length of the Group Description (Unsigned Long), Group Description (string)
    #   Group Creation Date (Unsigned Integer of 12 bytes)
    #
    #It is in Big Endian Order
    return None 
    # raw_group_name = bytes(groupName, 'utf-8')
    # raw_group_description = bytes(groupDescription = 'utf-8')
    # packed_data = struct.pack(">L%ds L%ds 12I", %(len(raw_group_name), len(raw_group_description)), \
    #         len(raw_group_name), raw_group_name, len(raw_group_description), raw_group_description, \
    #         creationDate)
    # return packed_data


def _message_sending(topic, group, message, isReply, isReplyTo, attachmentP):

    #This is the message sending code/structure. 
    #The Format of the Structure is designed specifically for ./Usenet
    #The format is:
    #
    #   Length of Topic (Unsigned Long), Topic
    #   Length of Group (Unsigned Long), Group
    #   Length of Message (Unsingned long) Message
    #   isReply(unsigned int 1 or 0), replyTo (Unsigned Integer) (0 = not a reply)
    #   hasAttachment (unsigned integer of 1 or 0)
    #
    #I am probably seperating the message logic from this. 
    #Note that this does not support attachments at this 
    #time. I am unsure of how to do attachments with this yet (uploading that is). 

    if isReply == True:
        isReply = 1
    elif isReply == False:
        isReply = 0
    if attachmentP == True:
        attachmentP = 1
    elif attachmentP == False:
        attachmentP = 0
    raw_topic = bytes(topic, 'utf-8')
    raw_group = bytes(group, 'utf-8')
    raw_message = bytes(message, 'utf-8')
    packed_data = struct.pack(">L%ds L%ds L%ds I L I" %(len(raw_topic), len(raw_group), \
        len(raw_message)), len(raw_topic), raw_topic, len(raw_group), raw_group, \
        len(raw_message), raw_message, isReply, isReplyTo, attachmentP)
    return packed_data


#Function - message_unpacking
#Args - Packed data
#Returns - String topic, String group, String message, Boolean isReply, Long isReplyTo, Boolean attachmentP
def _message_unpacking(data):

    #This is the unpacking method for unpacking messages. 
    #This works by reading each byte and then reconstructing the package peice by peice in a sense
    #Every 4 bytes before a string is the length, as defined by the Structure of Messages, excluding the
    #boolean values (which I use mock booleans as a way to not destory compatability. 

    topic = ""
    message = ""
    group = ""
    string_number = 1
    string_length= struct.unpack(">L", data[:4])
    string_length = string_length[0]
    current_byte = 4
    while True:
        string = struct.unpack("%ds" %string_length, data[current_byte:(string_length + current_byte)])
        if string_number == 1:
            topic = string[0].decode('utf-8')
            string_number = 2
        elif string_number == 2:
            group = string[0].decode('utf-8')
            string_number = 3
        elif string_number == 3:
            message = string[0].decode('utf-8')
            current_byte = string_length + current_byte
            break
        current_byte = string_length + current_byte
        string_length = struct.unpack(">L", data[current_byte: current_byte + 4])
        string_length = string_length[0]
        current_byte = current_byte + 4
    isReply = struct.unpack(">I", data[current_byte:(current_byte + 4)])
    current_byte += 4 
    isReplyTo = struct.unpack(">L", data[current_byte:(current_byte + 4)])
    current_byte += 4
    attachmentP = struct.unpack(">I", data[current_byte:])
    isReply = isReply[0]
    isReplyTo = isReplyTo[0]
    attachmentP = attachmentP[0]
    if isReply == 1:
        isReply = True
    elif isReply == 0:
        isReply = False
    if attachmentP == 1:
        attachmentP == True
    elif attachmentP == 0:
        attachmentP == False
    return topic, group, message, isReply, isReplyTo, attachmentP

def _group_unpacking(data):
    group_name = ""
    group_description = ""
    group_time = 0
    string_number = 0
    string_length = stuct.unpack(">L", data[:4])
    string_length = string_length[0]
    current_byte = 4
    while True:
        string = struct.unpack("%ds" %string_length, data[current_byte:(string_length + current_byte)])
        if string_number == 0:
            group_name = string[0].decode('utf-8')
            string_number = 1
        elif string_bumber == 1:
            group_name = string[0].decode('utf-8')
            current_byte = string_length + current_byte
            break
        current_byte = string_length + current_byte
        string_length = struct.unpack(">L", data[current_byte: current_byte + 4])
        string_length = string_length[0]
        current_byte = current_byte + 4
    group_time = struct.unpack(">12I", data[current_byte:])
    return group_name, group_description, group_time
