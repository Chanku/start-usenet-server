import connection_structs

#This is the official implementation of connection_structs.py for ./Usenet.
#This will also eventually be the way that protocols are called from as well.

#NOTE: Why do I have this? Is there any reason I abstracted this to a wrapper?
#       I have to wonder what I was thinking when I wrote this the way I did.

def message_sending(topic, group, message, isReply, isReplyTo, attachmentP):
    return connection_structs._message_sending(topic, group, message, isReply, isReplyTo, attachmentP)

def message_unpacking(data):
    return connection_structs._message_unpacking(data) #Returns 3 strings, 1 bool, 1 long, and 1 bool

def group_sending(groupName, groupDescription, creationDate = ""):
    return connection_structs._group_sending(groupName, groupDescription, creationDate)

def group_unpacking(data):
    return connection_structs._group_unpacking(data) #Returns 2 strings and an Integer

def listP_sending(groupName):
    return _post_list_sending(groupName)

def listP_requesting(groupName):
    return _post_list_requesting(groupName)


