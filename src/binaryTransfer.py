import socket
import os.path
import time
import connect

# This is for binary-file transfer. Ex: Sending/Recving a picture from a server
def _send_binary(s, file_path):
    string_filesize = ""
    is_safe = _check_file(file_path)
    if not is_safe:
        print("Connection tried getting a file with the wrong data")
        s.send(("Invalid File.").encode('utf-8'))
        return None
    try:
        filesize = os.path.getsize(file_path)
        string_filesize = str(filesize)
        s.send(string_filesize.encode('utf-8'))
    except OSError as e:
        print("Error with sending file size. Error is below")
        print(e)
        s.send(("ERR_OSE").encode('utf-8')) #ERR_OSE = Error: OSError.
    time.sleep(5)
    try:
        binary = open(file_path, 'rb')
        data_sent = 0
        sent_parts = binary.read(1024)
        while True:
            ds = s.send(sent_parts)
            data_sent = data_sent + ds
            if int(data_sent) >= int(string_filesize):
                print("File Transfer Complete.")
                break
            sent_parts = binary.read(1024)
        binary.close()
    except IOError as e:
        print("IOError when transferring file! Error is below.")
        print(e)
        s.send(("ERR_IOE").encode('utf-8')) #ERR_IOE = Error: IOError.

def _recv_binary(s, file_path):
    file_size = s.recv(1024).decode('utf-8')
    if "ERR" in file_path or "ERR" in file_size:
        return "F" #NOTE: What does F mean?
    try:
        f = open(file_path, 'wb')
        part = s.recv(1024)
        while True:
            f.write(part)
            f.flush()
            if int(os.path.getsize(file_path)) >= int(file_size):
                f.close()
                return "S"
            part = s.recv(1024)
    except (OSError, IOError) as e:
        print("OSError or IOError has beenc caught during File Upload")
        print(e)
    except:
        print("Unknown error has been caught during File Upload!")
