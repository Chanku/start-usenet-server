import struct
import socket

#NOTE: What is the purpose of this class actually? 
#       Shouldn't this be put in something like connection_structs.py?
class group_creation():
    #The group creation structure data is structed like this:
    #   group name
    #   group description
    def receving_structure(data):
        group_stuff = struct.unpack('">"', data)
        group_name = group_stuff[0]
        group_description = group_stuff[1]
        return group_name, group_description

    def sending_structure(group_name, group_description):
        rdGroup_name = group_name.encode('utf-8')
        rdGroup_description = group_description.encode('utf-8')
        data_to_send = struct.pack(">", rdGroup_name, rdGroup_description)
        print(data_to_send)
        return data_to_send

