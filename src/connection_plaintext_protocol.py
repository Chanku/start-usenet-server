import socket
import connection_client_format

def description_group_format(description_data):
    group, date, founder, _ = description_data.split('\n')
    day, month, date, time, year = date.split(" ")
    date = "{}, {} {}, {} at {}".format(day, month, date, year, time)
    return "Descrition\n\tGroup: {}\n\tFounded on: {}\n\t{}\n".format(group, date, founder)

def list_reply_formatting(data):
    if isinstance(data, str):
        return data
    try:
        return_string = ""
        for reply_id in data[2].split('\n'):
            if reply_id == "" or reply_id == "\n" or reply_id == "":
                return_string = return_string
            else:
                return_string = ("{}Group: {} || Index: {} || Topic:  Re: {}\n"
                                ).format(return_string, data[0], reply_id, data[1])
        return return_string[:(len(return_string) - 1)]
    except IndexError:
        return "Error in data formatting\n" 

def list_post_formatting(data):
    if isinstance(data, str):
        return data
    try:
        return_string = ""
        for post_data in data:
            for item in post_data:
                post_data[post_data.index(item)] = item.strip('\n')
            return_string = ("{}Group: {} || Index: {} || Topic:  {} || Poster: {} || Date: {}\n"
                            ).format(return_string, post_data[0], post_data[1], post_data[2],
                                     post_data[3], post_data[4])
        return return_string[:(len(return_string) - 1)]
    except IndexError:
        return "Error in data formatting\n" 

def client_connect(client):
    client.send("anonymous".encode('utf-8'))
    while True:
        rData = client.recv(2048)
        sData = rData.decode('utf-8')
        if "groupdesc" in sData.lower():
            #groupdesc [name]
            data = sData[10:]
            description_string = connection_client_format.client_group_desc(data)
            client.send(description_group_format(description_string).encode('utf-8'))
        elif "creations" in sData.lower():
            # This command is not in the official Protocol
            #  as such this is at risk at being removed at any time
            #  modify this or rely on this at your own risk!!!
            data = sData[10:]
            success_string = connection_client_format.client_create(data)
            client.send(success_string.encode('utf-8'))
        elif "postmakem" in sData.lower():
            #postmakem [group_name] :x:[topic] :x:[message]
            data = sData[10:]
            success_string = connection_client_format.client_post(data, 'anonymous')
            client.send(success_string.encode('utf-8'))
        elif "postreply" in sData.lower():
            #postreply [id] [group_name] :x: [reply]
            data = sData[10:]
            success_string = connection_client_format.client_reply(data, 'anonymous')
            client.send(success_string.encode('utf-8'))
        elif "postgetps" in sData.lower():
            #postgetps [group_name] :x: [id]
            data = sData[10:]
            success_string = connection_client_format.client_get_posts(*data.split(':x:'))
            client.send(success_string.encode('utf-8'))
        elif "postgetrp" in sData.lower():
            #postgetrp [group_name] :x: [id]
            data = sData[10:]
            reply_list = connection_client_format.client_get_replies(*data.split(':x:'))
            client.send(list_reply_formatting(reply_list).encode('utf-8'))
        elif "listposts" in sData.lower():
            #listposts [group_name]
            data = sData[10:]
            post_list = connection_client_format.client_list_posts(data)
            client.send(list_post_formatting(post_list).encode('utf-8'))
        elif "listgroup" in sData.lower():
            #listgroup
            success_string = connection_client_format.client_list_groups()
            client.send(success_string.encode('utf-8'))
        elif "endconnect" in sData.lower():
            #endconnect
            client.send("GOODBYE!\n".encode('utf-8'))
            client.close()
            exit()
