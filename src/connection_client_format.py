import threading
import connect
import connection_time
import os

#NOTE: REPLACE use of connect._check_dir() with something to check if the group is also registered as well

#This makes a group with the specified name/data
#Group names, or descriptions must not contain :x:
def client_create(data):
    print(data)
    spData = data.split(":x:")
    is_safe = connect._check_dir("./" + spData[0])
    if is_safe:
        if len(spData) > 2 or len(spData) < 2:
            return "Invalid Name or Description\n"
        else:
            while True:
                if spData[0].endswith(' '):
                    spData[0] = spData[0][:len(spData[0]) - 1]
                elif spData[0].startswith(' '):
                    spData[0] = spData[0][1:]
                else:
                    break
            try:
                os.makedirs(spData[0])
                os.makedirs('{}/resources'.format(spData[0]))
                fileThread = threading.Thread(target=connect._makeGroup, args=(spData[0], spData[1]))
                fileThread.start()
                return "Success\n"
            except FileExistsError:
                return "Already Exists\n"
    elif not is_safe:
        return "Invalid Name.\n"


#This section is the posting functionality
#see the posting message for more information
#It also removes the spaces before and after each section (as split bt the :x:) for some reason.
def remove_whitespace(split_data):
    s_data = []
    for item in split_data:
        while True:
            if item.startswith(' ') and item.endswith(' '):
                item = item[1:len(item)-1]
            elif item.endswith(' '):
                item = item[:len(item)-1]
            elif item.startswith(' '):
                item = item[1:]
            else:
                s_data.append(item)
                break
    del split_data
    split_data = []
    for item in s_data:
        split_data.append(item)
    del s_data
    return split_data

def client_post(data, username):
    split_data = data.split(':x:')
    if len(split_data) > 3 or len(split_data) < 3:
            return "Invalid Arguments\n"
    split_data = remove_whitespace(split_data)
    group = split_data[0]
    topic = split_data[1]
    message = split_data[2]
    is_safe = connect._check_dir('./{}/'.format(group))
    if is_safe:
        postThread = threading.Thread(target=connect._post, args=(group,username,topic,message))
        postThread.start()
        postThread.join()
        return "Success\n"
    return "Failure!"

def client_reply(data, username):
    #data = [group_name] :x: [id] :x:[reply]
    split_data = data.split(":x:")
    split_data = remove_whitespace(split_data)
    if len(split_data) > 3 or len(split_data) < 3:
        return "Invalid Arguements\n"
    group, post_id, reply = split_data
    if connect._check_dir('./{}/'.format(group)): 
        replyThread = threading.Thread(target=connect._reply, args=(group, username, post_id, reply))
        replyThread.start()
        replyThread.join()
        return "Success!\n"
    return "Failure!\n"

def client_group_desc(name):
    try:
        if connect._check_dir("./{}/".format(name)):
            f = open(("./{}/0.txt".format(name.strip('\t').strip('\n').strip('\r'))), 'r')
            desc_stuff = f.read()
            f.close()
            return desc_stuff 
        else:
            raise IOError
    except IOError:
        return "No such group\n"


def client_get_replies(group_name, post_id):
    group_name, post_id = remove_whitespace([group_name, post_id]) 
    post_id = post_id.strip("\r\n").strip('\n').strip('\r')
    try:
        if connect._check_dir('./{}/'.format(group_name)):
            if connect._check_dir('./{}/{}.txt'.format(group_name, post_id)):
                topic = ""
                with open('./{}/{}.txt'.format(group_name, post_id), 'r') as post:
                    topic = post.readline().strip('\n')
                with open('./{}/{}r.txt'.format(group_name, post_id), 'r') as post:
                    post_data = post.read()
                    if post_data == "" or post_data == "\n":
                        return "No Replies\n"
                    return [group_name, topic, post_data]
        else:
            raise IOError
    except IOError:
        return "No Such Post or Group\n"

def client_get_posts(group_name, post_id):
    #Add in checks here for important stuff like correct argument amount or against non-existant groups
    group_name, post_id = remove_whitespace([group_name, post_id]) 
    group_name = group_name.strip('\t').strip('\r\n').strip('\n').strip('\r')
    post_id = post_id.strip('\t').strip('\r\n').strip('\n').strip('\r')
    if connect._check_dir("./{}/".format(group_name)):
        if connect._check_dir("./{}/{}.txt".format(group_name, post_id)):
            try:
                with open('./{}/{}.txt'.format(group_name, post_id), 'r') as f:
                    response = f.read()
                topic, poster, time, message, _ = response.split('\n')
                return ("Group: {}\n"
                        "Poster: {}\n"
                        "Topic: {}\n"
                        "ID: {}\n"
                        "Time:{}\n"
                        "Message:\n"
                        "--------\n"
                        "{}\n").format(group_name, poster, topic, post_id, time, message)
            except (FileNotFoundError, IOError):
                raise
    return "File does not exist.\n"

def client_list_groups():
    try:
        with open('./group_directory.txt', 'r') as group_list:
            groups = ""
            for line in group_list:
                groups = "{}{}, ".format(groups, line.strip('\n').split(':x:')[0])
            return "{}\n".format(groups[:len(groups) - 2])
    except FileNotFoundError:
        return "The Server may be misconfigured or may have no groups\n"

def check_group(group_name):
    with open('./group_directory.txt', 'r') as group_list:
        groups = group_list.read()
        if group in groups:
            for line in group_list:
                if line.strip("\t").strip('\r').strip("\n") == group:
                    return True
    return False

def client_list_posts(group_name):
    post_information = [] 
    #Returns: 
    #   Poster
    #   Time/Date Posted
    #   Topic
    #   Reponses
    group_name_sanitized = group_name.strip("\r\n").strip('\r').strip('\n')
    try:
        if check_group and connect._check_dir('./{}/'.format(group_name)): 
            for post in os.listdir('./{}/'.format(group_name_sanitized)):
                if post != 'resources' and post != "0.txt" and (not post.endswith("r.txt") and
                                                                not post.endswith('u.txt')):
                    try:
                        with open('./{}/{}'.format(group_name_sanitized, post), 'r') as open_post:
                            post_data = [] #topic, poster, time
                            post_data.append(group_name_sanitized)
                            post_data.append(open_post.name.split('.')[1].split('/')[2])
                            for i in range(1,4):
                                post_data.append(open_post.readline())
                            post_information.append(post_data)
                    except IsADirectoryError:
                        pass
            return post_information
        else:
            return "Group Not Found Error\n"
    except FileNotFoundError:
        return "No groups seem to exist\n"
