import socket
import connect
import binaryTransfer

# Seems to be the first-written syncronization code that I wrote
# Will probably be rewritten to use the Structures I defined previously
#  as I'm going to use those for server-syncing...
def server_sync(host = "sapeinserv.us.to"):
    socket = _connect_to_server(host)
    group_data =  _get_remote_groups(socket)
    _get_remote_groups(socket)
    _compare_groups()
    socket.send(("disconnect").encode('utf-8'))
    socket.close()

def _connect_to_server(host):
    port = 44400
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        socket.connect((host,port))
    except IOError as e:
        print(e)
        print("Server to Server connection has failed...")
    return socket

def _compare_groups():
    f = open("./group_directory.txt")
    groups_local = f.read()
    f.close()

    f = open("./group_directory-REMOTE.txt")
    groups_remote = f.read()
    f.close()
    groups_local = groups_local.strip('\n')
    groups_remote = groups_remote.strip('\n')
    split_groups_local = groups_local.split("%%%OKAY-MATE%%%CHECK-CHECK%%%")
    split_groups_remote = groups_remote.split("%%%OKAY-MATE%%%CHECK-CHECK%%%")
    groups_to_add = []

    i = 1
    for group in split_groups_local:
        x = 1
        for remote_group in split_groups_remote:
            if (i % 2) == 0:
                break
            elif (x % 2) == 0:
                break

            if group == remote_group:
                groups_to_add.remove(group)
                break
            else:
                groups_to_add.append(remote_group)
            x += 1
        i += 1
    if len(groups_to_add > 0):
        for group in groups_to_add:
            is_safe = _check_dir("./" + group)
            if is_safe:
                try:
                    os.makedirs(split_data[1])
                    os.makedirs(split_data[1] + "/resources")
                    fileThread = threading.Thread(target=_makeGroup, args=(split_data[1], split_data[2]))
                    fileThread.start()
                    _makeGroup(group, split_groups_remote(i + 1))
                except:
                    print("Error occured in group creation.")

def _get_remote_groups(socket):
    socket.send("0x76")
    binaryTransfer._recv_binary(socket, "./group_directory-REMOTE.txt")

def send_remote_groups(socket):
    binaryTransfer._send_binart(socket, "./groups_directory.txt")
