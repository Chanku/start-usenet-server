import binaryTransfer
import connect_data
import connection_client_format
import connection_share
import connection_time
import os
import socket
import threading
import time

# The following seems to be the server implementation itself, will probably rewrite to be cleaner
#
#The entire client-server protocol has two versions, this is the plaintext one. It's designed to
# use strings, acting as commands, encoded with UTF-8/ASCII that are 9 characters long not
# including spaces. each command has it's own format with any format that allows for spaces in
# the names to have the character :x: to split it. There may be a space before, or after the :x:
# character.
#
#Example Command: postsmake [group_name] :x: [topic] :x: [message]
def client_connection(client):
    print("Client Connected") #remove in the future?
    raw_connection_type = client.recv(2048)
    is_plain = False
    connection = ""
    try:
        connection_type = raw_connection_type.decode('utf-8')
        if isinstance(connection_type, str):
            is_plain = True
            import connection_plaintext_protocol
            connection_plaintext_protocol.client_connect(client)
        else:
            pass #replace with binary connection
    except (AttributeError, UnicodeDecodeError):
        if is_plain:
            client.send("Uncaught Attribute or Unicode Error. You are disconnected.\n".encode('utf-8'))
            raise 
        else:
            pass #replace with binary connection
    # username = client.recv(2048).decode('utf-8')


def _groups():
    dirs = os.listdir()
    groups = ""
    for diri in dirs:
        is_safe = _check_dir("./" + groups + "/")
        if not is_safe:
            if groups == "":
                groups = "HACKER!!!"
            elif groups != "":
                groups = groups + "%%%^%%%" + "lel"
        if diri != ".git" and diri != "__pycache__":
            if os.path.isdir(diri):
                if groups == "":
                    groups = diri
                elif groups != "":
                    groups = groups + "%%%^%%%" + diri
    return groups


#WTF was this for?
def _sizeof(string):
    string_size == sys.getsizeof(string)
    if string_size > 2048:
        return 0x55
    elif string_size <= 2048:
        return 0x66

#TODO: Remove this function
def _post2(group, poster, topic, message):
    #NOTE: This is just a wrapper around _post,
    #       as such beware that this may be removed
    #       use at your own risk!
    return _post(group, poster, topic, message)

def _post(group, poster, topic, message):
    i = 1
    while True:
        fileStuff = './' + group + '/' + str(i) + '.txt'
        fileStuff2 = './' + group + '/' + str(i) + 'u.txt'
        is_safe = _check_dir('./' + group + "/")
        if is_safe:
            try:
                f = open(fileStuff, 'r')
                f.close()
                i += 1
            except IOError:
                try:
                    f = open(fileStuff2, 'r')
                    f.close()
                    i += 1
                except IOError:
                    f = open(fileStuff2, 'w')
                    f.write(topic + '\n')
                    f.write(poster + '\n')
                    f.write(connection_time.get_SNTP_time() + '\n')
                    f.write(message)
                    f.close()
                    os.rename(fileStuff2, fileStuff)
                    with open('{}r.txt'.format(fileStuff[:len(fileStuff) - 4]), 'w') as f:
                        f.write('')
                    return i
    print("done")


def _reply(group, poster, original_id, message):
    with open('./{}/{}.txt'.format(group, original_id), 'r') as original_post:
        topic = original_post.readline().strip("\n")
    topic = 'Re: {}'.format(topic)
    post_id = _post(group, poster, topic, message)
    with open('./{}/{}r.txt'.format(group, original_id), 'a') as original_post:
        original_post.write('{}\n'.format(post_id))
    print("Done")


def _makeGroup(groupName, description):
    directory = open("./group_directory.txt", 'a')
    while True:
        if description.startswith(' '):
            description = description[1:]
        elif description.endswith(' '):
            description = description[:len(description) - 1]
        elif groupName.endswith(' '):
            groupName = groupName[:len(groupName) - 1]
        elif groupName.startswith(' '):
            groupName = groupName[1:]
        else:
            break
    if description.endswith('\n'):
        directory.write("{}:x:{}".format(groupName, description))
    elif not description.endswith('\n'):
        directory.write("{}:x:{}\n".format(groupName, description))
    directory.close()

    fileStuff = "./" + groupName + "/0.txt"
    firstFile = open(fileStuff, "w")
    firstFile.write(groupName + "\n")
    firstFile.write(str(connection_time.get_SNTP_time()) +  "\n")
    firstFile.write(description)
    firstFile.close()

def get_NTP_Time():
    server = "pool.ntp.org"
    port = 123

def _data_size(s):
    size_of_data = s.recv(2048)
    data = s.recv(2048).decode('utf-8')
    size_of_data_recviced = sys.getsizeof(data)
    while True:
        if int(size_of_data) == int(size_of_data_recived):
            data = data_stored
            break
        data_stored = data_stored + data
        data = s.recv(2048).decode('utf-8')
        size_of_data_recviced = sys.getsizeof(data)
    return data

def _send_file(s, group, file_name):
    print("Test")
    file_path = "./" + group + "/resources/" + file_name
    split_filename = file_name.split("~|~")
    s.send(split_filename[1].encode('utf-8'))
    binaryTransfer._send_binary(s, file_path)

def _take_file(s, group, file_name, i):
    file_path = "./" + group + "/resources/" + str(i) + "b~|~" +  file_name
    binaryTransfer._recv_binary(s, file_path)

def _check_dir(path):
    safe_dir = os.path.abspath(os.curdir)
    path = os.path.abspath(path)
    common_path = _common_prefix([safe_dir, path])

    if safe_dir in common_path:
        return True
    elif safe_dir not in common_path:
        return False
    else:
        return False

def _timer():
    time.sleep(3600)
    return True

#Note the default is just a temporary default, this will change in the future
def sync_servers(host = "sapeinserv.us.to"):
    while True:
        time_to_sync = _timer()
        if time_to_sync:
            connect-share.server_sync()

def _common_prefix(path):
    common_path = []
    split_path = [path_part.split('/') for path_part in path]
    minimum_length = min( len(p) for p in split_path)
    for i in range(minimum_length):
        s = set(p[i] for p in split_path)
        if len(s) != 1:
            break
        common_path.append(s.pop())
    return '/'.join(common_path)
